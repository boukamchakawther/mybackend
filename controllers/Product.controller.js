const ProductModel = require("../models/Product.model")

module.exports = {

    creat: function (req, res) {

    
        const Product = new ProductModel(req.body)
        req.body['galleries'] ={
            name: req.file.filename,
            description:"eeeeee"
        }
        Product.save(function (err, item) {

            if (err) {
                res.status(406).json({ status: 406, message: 'error create Product'+err, data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create Product', data: item })
            }

        })
    },
    update: function (req, res) {
        ProductModel.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error update Product', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create update', data: item })
            }
        })
    },
    delete: function (req, res) {
       ProductModel.findByIdAndRemove(req.params.id, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error delete Product', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' delete Product', data: item })
            }
        })
    
    },
    find: function (req, res) {
       ProductModel.find({},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find Product', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find all Product', data: item })
            }
        })
    
    },
    getOne: function (req, res) {
       ProductModel.findById(req.params.id,function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by id Product', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by id Product', data: item })
            }
        })
    
    },
    findByEmail: function (req, res) {
      ProductModel.find({email:req.body.email},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by email', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by email', data: item })
            }
        })
    
    },
}
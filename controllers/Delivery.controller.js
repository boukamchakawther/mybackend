const userModel = require("../models/delivery.model")

module.exports = {

    creat: function (req, res) {
        const delivery = new userModel(req.body)
        delivery.save(function (err, item) {

            if (err) {
                res.status(406).json({ status: 406, message: 'error create delivery', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create delivery', data: item })
            }

        })
    },
    update: function (req, res) {
        userModel.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error update delivery', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create update', data: item })
            }
        })
    },
    delete: function (req, res) {
        userModel.findByIdAndRemove(req.params.id, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error delete delivery', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' delete delivery', data: item })
            }
        })
    
    },
    find: function (req, res) {
        userModel.find({},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find delivery', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find all delivery', data: item })
            }
        })
    
    },
    getOne: function (req, res) {
        userModel.findById(req.params.id,function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by id delivery', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by id delivery', data: item })
            }
        })
    
    },
    findByEmail: function (req, res) {
        userModel.find({email:req.body.email},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by email', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by email', data: item })
            }
        })
    
    },
}
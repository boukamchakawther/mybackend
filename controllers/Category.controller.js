const CategoryModel = require("../models/Category.model")

module.exports = {

    create: function (req, res) {
        req.body['image'] = req.file.filename;
        const Category = new CategoryModel(req.body)
        Category.save(function (err, item) {

            if (err) {
                res.status(406).json({ status: 406, message: 'error create Category', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create Category', data: item })
            }

        })
    },
    update: function (req, res) {
        CategoryModel.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error update Category', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create update', data: item })
            }
        })
    },
    delete: function (req, res) {
        CategoryModel.findByIdAndRemove(req.params.id, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error delete Category', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' delete Category', data: item })
            }
        })
    
    },
    find: function (req, res) {
        CategoryModel.find({},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find Category', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find all Category', data: item })
            }
        })
    
    },
    getOne: function (req, res) {
        CategoryModel.findById(req.params.id,function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by id Category', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by id Category', data: item })
            }
        })
    
    },
    findByEmail: function (req, res) {
        CategoryModel.find({email:req.body.email},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by email', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by email', data: item })
            }
        })
    
    },
}
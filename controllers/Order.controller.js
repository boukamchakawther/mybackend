const OrderModel = require("../models/Order.model");

module.exports = {
  create: async function (req, res, next) {
    console.log(req.body)
    const order = new OrderModel(req.body)
    order.save(req.body, async function (err, item) {
        if (err) {
            res.status(406).json({ status: 406, message: err.message, data: null })
        } else {
            const client = await ClientModel.findById({ _id: item.client });
            client.orders.push(item)
            const c=await client.save()
            console.log("client", c)
            res.status(201).json({ status: 201, message: 'create user', data: item })
        }
    })
},
  update: function (req, res) {
    OrderModel.findByIdAndUpdate(
      req.params.id,
      req.body,
      { new: true },
      function (err, item) {
        if (err) {
          res
            .status(406)
            .json({ status: 406, message: "error update Order", data: null });
        } else {
          res
            .status(201)
            .json({ status: 201, message: "create update", data: item });
        }
      }
    );
  },
  delete: function (req, res) {
    OrderModel.findByIdAndRemove(req.params.id, function (err, item) {
      if (err) {
        res
          .status(406)
          .json({ status: 406, message: "error delete Order", data: null });
      } else {
        res
          .status(200)
          .json({ status: 200, message: " delete Order", data: item });
      }
    });
  },
  find: function (req, res) {
    OrderModel.find({}, function (err, item) {
      if (err) {
        res
          .status(406)
          .json({ status: 406, message: "error find Order", data: null });
      } else {
        res
          .status(200)
          .json({ status: 200, message: " find all Order", data: item });
      }
    })
      .populate("client")
      .populate({
        path: "products",
        populate: "product",
      });
  },
  getOne: function (req, res) {
    OrderModel.findById(req.params.id, function (err, item) {
      if (err) {
        res
          .status(406)
          .json({ status: 406, message: "error find by id Order", data: null });
      } else {
        res
          .status(200)
          .json({ status: 200, message: " find by id Order", data: item });
      }
    });
  },
  findByEmail: function (req, res) {
    OrderModel.find({ email: req.body.email }, function (err, item) {
      if (err) {
        res
          .status(406)
          .json({ status: 406, message: "error find by email", data: null });
      } else {
        res
          .status(200)
          .json({ status: 200, message: " find by email", data: item });
      }
    });
  },
};

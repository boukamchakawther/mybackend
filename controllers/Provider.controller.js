const ProviderModel = require("../models/Provider.model")

module.exports = {

    creat: function (req, res) {
     //console.log("req.body")
        const provider = new ProviderModel(req.body)
        provider.save(function (err, item) {

            if (err) {
                res.status(406).json({ status: 406, message: 'error create provider'+ err, data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create provider', data: item })
            }

        })
    },
    update: function (req, res) {
        ProviderModel.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error update provider', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create update', data: item })
            }
        })
    },
    delete: function (req, res) {
       ProviderModel.findByIdAndRemove(req.params.id, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error delete provider', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' delete provider', data: item })
            }
        })
    
    },
    find: function (req, res) {
        providerModel.find({},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find provider', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find all provider', data: item })
            }
        })
    
    },
    getOne: function (req, res) {
        providerModel.findById(req.params.id,function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by id provider', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by id provider', data: item })
            }
        })
    
    },
    findByEmail: function (req, res) {
        providerModel.find({email:req.body.email},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by email', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by email', data: item })
            }
        })
    
    },
}
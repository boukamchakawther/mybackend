const GalleryModel = require("../models/Gallery.model")

module.exports = {
    req.body['image'] = req.file.filename;
    creat: function (req, res) {
        const Gallery = new galleryModel(req.body)
        Gallery.save(function (err, item) {

            if (err) {
                res.status(406).json({ status: 406, message: 'error create Gallery', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create Gallery', data: item })
            }

        })
    },
    update: function (req, res) {
        GalleryModel.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error update Gallery', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create update', data: item })
            }
        })
    },
    delete: function (req, res) {
        GalleryModel.findByIdAndRemove(req.params.id, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error delete Gallery', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' delete Gallery', data: item })
            }
        })
    
    },
    find: function (req, res) {
        GalleryModel.find({},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find Gallery', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find all Gallery', data: item })
            }
        })
    
    },
    getOne: function (req, res) {
        GalleryModel.findById(req.params.id,function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by id Gallery', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by id Gallery', data: item })
            }
        })
    
    },
    findByEmail: function (req, res) {
        GalleryModel.find({email:req.body.email},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by email', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by email', data: item })
            }
        })
    
    },
}
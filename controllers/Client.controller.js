const clientModel = require("../models/client.model");
const userModel = require("../models/client.model")

module.exports = {

    creat: function (req, res) {
        req.body['image'] = req.file.filename;
        const client = new userModel(req.body)
        client.save(function (err, item) {

            if (err) {
                res.status(406).json({ status: 406, message: 'error create client*'+err, data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create client', data: item })
            }

        })
    },
    update: function (req, res) {
        userModel.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error update client', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create update', data: item })
            }
        })
    },
    
    delete: function (req, res) {
        userModel.findByIdAndRemove(req.params.id, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error delete client', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' delete client', data: item })
            }
        })
    
    },
    find: async function (req, res){
       /* userModel.find({},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find client', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find all client', data: item })
            }
        })*/
        const clients= await userModel.find({})

        res.status(200).json({ status: 200, message: ' find all client', data: clients })



    },
    getOne:  async function (req, res) {
       /* userModel.findById(req.params.id,function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by id client', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by id client', data: item })
            }
        })*/
        const client=await clientModel.findById(req.params.id).select('phone lastName firstName')
        
        res.status(200).json({ status: 200, message: ' find all client', data: client})
    
    },
    findByEmail: function (req, res) {
        userModel.find({email:req.body.email},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by email', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by email', data: item })
            }
        })
    
    },
}
const SubCategoryModel = require("../models/SubCategory.model")

module.exports = {

    creat: function (req, res) {
        const SubCategory = new SubCategoryModel(req.body)
        SubCategory.save(function (err, item) {

            if (err) {
                res.status(406).json({ status: 406, message: 'error create SubCategory', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create SubCategory', data: item })
            }

        })
    },
    update: function (req, res) {
        SubCategoryModel.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error update SubCategory', data: null })
            } else {
                res.status(201).json({ status: 201, message: 'create update', data: item })
            }
        })
    },
    delete: function (req, res) {
        SubCategoryModel.findByIdAndRemove(req.params.id, function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error delete SubCategory', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' delete SubCategory', data: item })
            }
        })
    
    },
    find: function (req, res) {
       SubCategoryModel.find({},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find SubCategory', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find all SubCategory', data: item })
            }
        }) .populate("category","-__v").select("-__v")
    
    },
    getOne: function (req, res) {
        sSubCategoryModel.findById(req.params.id,function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by id SubCategory', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by id SubCategory', data: item })
            }
        })
    
    },
    findByEmail: function (req, res) {
        SubCategoryModel.find({email:req.body.email},function (err, item) {
            if (err) {
                res.status(406).json({ status: 406, message: 'error find by email', data: null })
            } else {
                res.status(200).json({ status: 200, message: ' find by email', data: item })
            }
        })
    
    },
}
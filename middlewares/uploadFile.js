const multer = require("multer")
const storage = multer.diskStorage(
    {
        destination: function (req, file, cb) {
            cb(null, './storages')
        }
        ,
        filename: function (req, file, cb) {
            cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname.replace(/\s/g,''));


        }
    }
)
module.exports = multer({
    storage: storage
   /* limits: { _fileSize: 1024 * 1024 * 10 } , //10mb
    fileFilter: function (req, file, cb) {
        if (file.mimetype === "image/jpg" ||
            file.mimetype === "image/jpeg" ||
            file.mimetype === "image/png") {
            cb(null, true);
        } else {
            cb(new Error("Image uploaded is not of type jpg/jpeg or png"), false);
        }
    }*/

})
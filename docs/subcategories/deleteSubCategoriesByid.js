module.exports = {

    delete: {
        tags: ["subcategories"],
        description: "delete",
        operationId: "deletesubcategories",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "deleting a done subcategories"

            }


        ],
    }

}
module.exports = {

    get: {
        tags: ["subcategories"],
        description: "Get All",
        operationId: "getAllsubcategories",
        parameters: [],
        responses: {
            '200': {

                description: "get all",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/ApiResponse'
                        }
                    }
                }
            }
        }

    }
}
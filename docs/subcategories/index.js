const getAllSubCategories = require("./getAllSubCategories")
const createSubCategories = require("./createSubCategories")
const deleteSubCategoriesById = require("./deleteSubCategoriesByid")
const updateSubCategories = require("./updateSubCategories")

module.exports = {

        "/SubCategories": {
            ...getAllSubCategories,
            ...createSubCategories,

        },
        "/SubCategories/{id}": {
            ...deleteSubCategoriesById,
            ...updateSubCategories,

        },       
}
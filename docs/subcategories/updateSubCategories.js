module.exports = {

    put: {
        tags: ["subcategories"],
        description: "update",
        operationId: "updatesubcategories",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "update a done subcategories"

            }


        ],
        requestBody: {
            //content type
            content: {
                "application/json": {
                    schema: {
                        $ref: '#/components/schemas/Providersinput'
                    }

                }
            }

        },
        responses: {
            '200': {
                description: "update subcategories ",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            },
            '401': {
                description: "no token",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                },


            },
            '406': {
                description: "error update subcategories",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            }
        },
    }
}
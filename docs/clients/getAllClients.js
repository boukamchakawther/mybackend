module.exports = {

    get: {
        tags: ["Client"],
        description: "Get All",
        operationId: "getAllClient",
        parameters: [],
        responses: {
            '200': {

                description: "get all",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/ApiResponse'
                        }
                    }
                }
            }
        }

    }
}
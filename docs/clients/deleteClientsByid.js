module.exports = {

    delete: {
        tags: ["Client"],
        description: "delete",
        operationId: "deleteclient",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "deleting a done client"

            }


        ],
    }

}
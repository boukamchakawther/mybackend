const getAllClients = require("./getAllClients")
const createClients = require("./createClients")
const deleteClientsById = require("./deleteClientsById")
const updateClients = require("./updateClients")

module.exports = {

        "/clients": {
            ...getAllClients,
            ...createClients,

        },
        "/clients/{id}": {
            ...deleteClientsById,
            ...updateClients,

        },       
}
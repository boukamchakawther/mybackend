const getAllCategory = require("./getAllCategory")
const createCategory = require("./createCategory")
const deleteCategoryById = require("./deleteCategoryById")
const updateCategory = require("./updateCategory")

module.exports = {

        "/categories": {
            ...getAllCategory,
            ...createCategory,

        },
        "/categories/{id}": {
            ...deleteCategoryById,
            ...updateCategory,

        },       
}
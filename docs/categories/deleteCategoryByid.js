module.exports = {

    delete: {
        tags: ["Category"],
        description: "delete",
        operationId: "deleteCategory",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "deleting a done category"

            }


        ],
    }

}
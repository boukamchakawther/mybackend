module.exports = {

    put: {
        tags: ["Category"],
        description: "update",
        operationId: "updateCategory",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "update a done category"

            }


        ],
        requestBody: {
            //content type
            content: {
                "application/json": {
                    schema: {
                        $ref: '#/components/schemas/categoryinput'
                    }

                }
            }

        },
        responses: {
            '200': {
                description: "update category ",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            },
            '401': {
                description: "no token",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                },


            },
            '406': {
                description: "error update user",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            }
        },
    }
}
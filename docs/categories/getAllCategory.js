module.exports = {

    get: {
        tags: ["Category"],
        description: "Get All",
        operationId: "getAllCategory",
        parameters: [],
        responses: {
            '200': {

                description: "get all",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/ApiResponse'
                        }
                    }
                }
            }
        }

    }
}
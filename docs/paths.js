const categories = require("./categories")
const clients = require("./clients")
const users = require("./users")
const deliveries = require("./deliveries")
const orders = require("./orders")
const products = require("./products")
const providers = require("./providers")
const subcategories = require("./subcategories")
const galleries = require("./galleries")


module.exports = {
    paths: {
        ...categories,
        ...clients,
        ...users,
        ...deliveries,
        ...galleries,
        ...orders,
        ...products,
        ...providers,
        ...subcategories,
        ...galleries
    }
}
const getAllProducts = require("./getAllProducts")
const createProducts = require("./createProducts")
const deleteProductsById = require("./deleteProductsById")
const updateProducts = require("./updateProducts")

module.exports = {

    "/Products": {
        ...getAllProducts,
        ...createProducts,

    },
    "/Products/{id}": {
        ...deleteProductsById,
        ...updateProducts,

    },
}
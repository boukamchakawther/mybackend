module.exports = {

    delete: {
        tags: ["Products"],
        description: "delete",
        operationId: "deleteProducts",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "deleting a done Products"

            }


        ],
    }

}
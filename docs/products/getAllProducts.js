module.exports = {

    get: {
        tags: ["Products"],
        description: "Get All",
        operationId: "getAllProducts",
        parameters: [],
        responses: {
            '200': {

                description: "get all Products",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/ApiResponse'
                        }
                    }
                }
            }
        }

    }
}
module.exports = {

    get: {
        tags: ["galleries"],
        description: "Get All",
        operationId: "getAllgalleries",
        parameters: [],
        responses: {
            '200': {

                description: "get all",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/ApiResponse'
                        }
                    }
                }
            }
        }

    }
}
const getAllgalleries = require("./getAllgalleries")
const creategalleries = require("./creategalleries")
const deletegalleriesById = require("./deletegalleriesById")
const updategalleries = require("./updategalleries")

module.exports = {

        "/galleries": {
            ...getAllgalleries,
            ...creategalleries,

        },
        "/galleries/{id}": {
            ...deletegalleriesById,
            ...updategalleries,

        },       
}
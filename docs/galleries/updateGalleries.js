module.exports = {

    put: {
        tags: ["galleries"],
        description: "update",
        operationId: "updategalleries",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "update a done galleries"

            }


        ],
        requestBody: {
            //content type
            content: {
                "application/json": {
                    schema: {
                        $ref: '#/components/schemas/galleriesinput'
                    }

                }
            }

        },
        responses: {
            '200': {
                description: "update galleries ",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            },
            '401': {
                description: "no token",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                },


            },
            '406': {
                description: "error update galleries",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            }
        },
    }
}
module.exports = {

    delete: {
        tags: ["galleries"],
        description: "galleries",
        operationId: "deletegalleries",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "deleting a done galleries"

            }


        ],
    }

}
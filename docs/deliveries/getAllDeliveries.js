module.exports = {

    get: {
        tags: ["deliveries"],
        description: "Get All",
        operationId: "getAlldeliveries",
        parameters: [],
        responses: {
            '200': {

                description: "get all",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/ApiResponse'
                        }
                    }
                }
            }
        }

    }
}
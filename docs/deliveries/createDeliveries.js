module.exports = {
    post: {
        tags: ["deliveries"],
        description: 'in deliveries',
        operationId: "createdeliveries",
        parameters: [],
        requestBody: {
            //content type
            content: {
                "application/json": {
                    schema: {
                        $ref: '#/components/schemas/deliveriesinput'
                    }

                }
            }

        },
        responses: {
            '200': {
                description: "create deliveries ",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            },
            '401': {
                description: "no token",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                },


            },
            '406': {
                description: "error create deliveries",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            }
        },
    }
}
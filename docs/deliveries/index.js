const getAllDeliveries = require("./getAllDeliveries")
const createDeliveries = require("./createDeliveries")
const deleteDeliveriesById = require("./deleteDeliveriesById")
const updateDeliveries = require("./updateDeliveries")

module.exports = {

        "/Deliveries": {
            ...getAllDeliveries,
            ...createDeliveries,

        },
        "/Deliveries/{id}": {
            ...deleteDeliveriesById,
            ...updateDeliveries,

        },       
}
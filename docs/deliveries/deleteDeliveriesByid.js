module.exports = {

    delete: {
        tags: ["deliveries"],
        description: "delete",
        operationId: "deletedeliveries",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "deleting a done deliveries"

            }


        ],
    }

}
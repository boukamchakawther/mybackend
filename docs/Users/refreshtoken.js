module.exports = {

    post: {
        tags: ["Users"],
        description: "refreshtoken",
        operationId: "refreshtokenUsers",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "ref a done Users"

            }


        ],
    }

}
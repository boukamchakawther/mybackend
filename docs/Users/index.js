const login = require("./login")
const logout = require("./logout")
const refreshtoken = require("./refreshtoken")


module.exports = {

    "/users": {
        ...login
       
    },
    "/users/logout": {
        ...logout
       
    },
    "/users/refreshtoken": {
        ...refreshtoken
       
    },

} 
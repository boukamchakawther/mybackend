module.exports = {

    post: {
        tags: ["Users"],
        description: "logout",
        operationId: "getlogout",
        parameters: [],
        requestBody: {
            //content type
            content: {
                "application/json": {
                    schema: {
                        $ref: '#/components/schemas/logout'
                    }

                }
            }

        },
        responses: {
            '200': {

                description: "get all Users",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/logout'
                        }
                    }
                }
            }
        }

    }
}
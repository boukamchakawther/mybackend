module.exports = {
    components: {
        schemas: {
            id: {
                type: 'string',
                description: "in of a category"
            },
            ApiResponses: {
                type: 'objet',
                properties: {
                    status: {

                        type: 'string'
                    },
                    message: {
                        type: 'string'

                    },
                    data: {

                        type: 'array',
                        items: {

                        }
                    }
                }
            },
            category: {
                type: 'object',
                properties: {
                    id: {
                        type: 'string',
                    },
                    name: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    }

                }

            },
            categoryinput: {
                type: 'object',
                properties: {

                    name: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    }

                }

            },
            SubCategory: {
                type: 'object',
                properties: {

                    name: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    },
                    category: {
                        type: 'string'

                    },
                    Product: {
                        type: 'string'

                    },
                }
            },
            SubCategoryinput: {
                type: 'object',
                properties: {

                    name: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    },
                    category: {
                        type: 'string'

                    },
                    Product: {
                        type: 'string'

                    },

                }
            },
            Order: {
                type: 'object',
                properties: {

                    ref: {
                        type: 'string'

                    },
                    priceTotal: {
                        type: 'string'

                    },
                    dateOrder: {
                        type: 'string'

                    },
                    QteTotal: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    },
                    client: {
                        type: 'string'

                    },
                    product: {
                        type: 'string'

                    },
                    delivery: {
                        type: 'string'

                    },

                }
            },
            Orderinput: {
                type: 'object',
                properties: {

                    ref: {
                        type: 'string'

                    },
                    priceTotal: {
                        type: 'string'

                    },
                    dateOrder: {
                        type: 'string'

                    },
                    QteTotal: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    },
                    client: {
                        type: 'string'

                    },
                    product: {
                        type: 'string'

                    },
                    delivery: {
                        type: 'string'

                    },

                }
            },
            Product: {
                type: 'object',
                properties: {

                    price: {
                        type: 'string'

                    },
                    name: {
                        type: 'string'

                    },
                    refproduct: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    },
                    qte: {
                        type: 'string'

                    },
                    galleries: {
                        type: 'string'

                    },
                    orders: {
                        type: 'string'

                    },
                    providers: {
                        type: 'string'

                    },
                    SubCategory: {
                        type: 'string'

                    },
                }
            },
            Productinput: {
                type: 'object',
                properties: {

                    price: {
                        type: 'string'

                    },
                    name: {
                        type: 'string'

                    },
                    refproduct: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    },
                    qte: {
                        type: 'string'

                    },
                    galleries: {
                        type: 'string'

                    },
                    orders: {
                        type: 'string'

                    },
                    providers: {
                        type: 'string'

                    },
                    SubCategory: {
                        type: 'string'

                    },
                }
            },
            Clients: {
                type: 'object',
                properties: {
                    id: {
                        type: 'string',
                    },
                    firstName: {
                        type: 'string'

                    },
                    lastName: {
                        type: 'string'

                    },
                    email: {
                        type: 'string'

                    },
                    password: {
                        type: 'string'

                    },
                    phone: {
                        type: 'number'

                    },
                    adresseL: {
                        type: 'string'

                    },

                }

            },
            clientsinput: {
                type: 'object',
                properties: {

                    firstName: {
                        type: 'string'

                    },
                    lastName: {
                        type: 'string'

                    },
                    email: {
                        type: 'string'

                    },
                    password: {
                        type: 'string'

                    },
                    phone: {
                        type: 'number'

                    },
                    adresseL: {
                        type: 'string'


                    }

                },



            },
            Providers: {
                type: "object",
                properties: {
                    firstName: {
                        type: "string"
                    },
                    lastName: {
                        type: "string"
                    },
                    email: {
                        type: "string"
                    },
                    password: {
                        type: "string"
                    },
                    adress: {
                        type: "string"
                    },
                    tel: {
                        type: "string"
                    },
                    photo: {
                        type: "file",
                        format: "binary"
                    },
                    company: {
                        type: "string"
                    },
                    products: {
                        type: "array",
                        items: {
                            type: "string"
                        }
                    }
                }
            },
            ProvidersInput: {
                type: "object",
                properties: {
                    firstName: {
                        type: "string"
                    },
                    lastName: {
                        type: "string"
                    },
                    email: {
                        type: "string"
                    },
                    password: {
                        type: "string"
                    },
                    adress: {
                        type: "string"
                    },
                   phone: {
                        type: "string"
                    },
                    photo: {
                        type: "file",
                        format: "binary"
                    },
                    company: {
                        type: "string"
                    },
                    products: {
                        type: "array",
                        items: {
                            type: "string"
                        }
                    }
                }
            },
            Delivery: {
                type: 'object',
                properties: {

                    num: {
                        type: 'Number'

                    },
                    firstName: {
                        type: 'string'

                    },
                    lastName: {
                        type: 'string'

                    },
                    email: {
                        type: 'string'

                    },
                    password: {
                        type: 'string'

                    },
                    phone: {
                        type: 'number'

                    },
                    ref: {
                        type: 'string'

                    },
                    priceTotal: {
                        type: 'string'

                    },
                    dateOrder: {
                        type: 'string'

                    },
                    QteTotal: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    },
                }
            },
            Deliveryinput: {
                type: 'object',
                properties: {

                    num: {
                        type: 'Number'

                    },
                    firstName: {
                        type: 'string'

                    },
                    lastName: {
                        type: 'string'

                    },
                    email: {
                        type: 'string'

                    },
                    password: {
                        type: 'string'

                    },
                    phone: {
                        type: 'number'

                    },
                    ref: {
                        type: 'string'

                    },
                    priceTotal: {
                        type: 'string'

                    },
                    dateOrder: {
                        type: 'string'

                    },
                    QteTotal: {
                        type: 'string'

                    },
                    description: {
                        type: 'string'

                    },
                }
            },
            Users: {
                type: 'object',
                properties: {
                    id: {
                        type: 'string',
                    },
                    firstName: {
                        type: 'string'

                    },
                    lastName: {
                        type: 'string'

                    },
                    email: {
                        type: 'string'

                    },
                    password: {
                        type: 'string'

                    },
                    phone: {
                        type: 'number'

                    },

                },


            },
            loginUsersinput: {
                type: 'object',
                properties: {

                    email: {
                        type: 'string'

                    },
                    password: {
                        type: 'string'

                    },
                   

                }

            },
            logout: {
                type: 'object',
                properties: {

                    refreshtoken: {
                        type: 'string'

                    },
                   
                   

                }

            },
        }
    }
}
const getAllProviders = require("./getAllProviders")
const createProviders = require("./createProviders")
const deleteProvidersById = require("./deleteProvidersByid")
const updateProviders = require("./updateProviders")

module.exports = {

        "/Providers": {
            ...getAllProviders,
            ...createProviders,

        },
        "/Providers/{id}": {
            ...deleteProvidersById,
            ...updateProviders,

        },       
}
module.exports = {
    post: {
        tags: ["Providers"],
        description: 'in providers',
        operationId: "createProviders",
        parameters: [],
        requestBody: {
            //content type
            content: {
                "application/json": {
                    schema: {
                        $ref: '#/components/schemas/ProvidersInput'
                    }

                }
            }

        },
        responses: {
            '200': {
                description: "create Providers ",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            },
            '401': {
                description: "no token",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                },


            },
            '406': {
                description: "error create Providers",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#/components/schemas/ApiResponse'
                        }
                    }
                }
            }
        },
    }
}
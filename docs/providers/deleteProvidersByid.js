module.exports = {

    delete: {
        tags: ["Providers"],
        description: "delete",
        operationId: "deleteProviders",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "deleting a done Providers"

            }


        ],
    }

}
module.exports = {

    get: {
        tags: ["Providers"],
        description: "Get All",
        operationId: "getAllProviders",
        parameters: [],
        responses: {
            '200': {

                description: "get all",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/ApiResponse'
                        }
                    }
                }
            }
        }

    }
}
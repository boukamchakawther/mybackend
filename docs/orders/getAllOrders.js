module.exports = {

    get: {
        tags: ["orders"],
        description: "Get All",
        operationId: "getAllorders",
        parameters: [],
        responses: {
            '200': {

                description: "get all",
                content: {
                    'application/json': {
                        schema: {
                            $ref: '#components/schemas/ApiResponse'
                        }
                    }
                }
            }
        }

    }
}
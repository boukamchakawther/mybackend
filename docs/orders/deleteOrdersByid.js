module.exports = {

    delete: {
        tags: ["orders"],
        description: "delete orders",
        operationId: "deleteorders",
        parameters: [
            {

                name: 'id',
                in: "path",
                schema: {
                    $ref: "#/components/schemas/id"

                },
                required: true,
                description: "deleting a done orders"

            }


        ],
    }

}
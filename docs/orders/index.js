const getAllOrders = require("./getAllOrders")
const createOrders = require("./createOrders")
const deleteOrdersById = require("./deleteOrdersById")
const updateOrders = require("./updateOrders")

module.exports = {

        "/Orders": {
            ...getAllOrders,
            ...createOrders,

        },
        "/Orders/{id}": {
            ...deleteOrdersById,
            ...updateOrders,

        },       
}
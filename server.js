const { application } = require('express')
const express = require('express')

const cors=require("cors")
//config swagger
const swaggerUI = require("swagger-ui-express")
const docs = require("./docs")
const basicAuth = require('express-basic-auth');


const app = express()
const port = 3000
app.use(cors("*"))
const db = require("./config/database")
const UserRouter = require("./routers/User.router")
const ClientRouter = require("./routers/Client.router")
const ProviderRouter = require("./routers/Provider.router")
const DeliveryRouter = require("./routers/Delivery.router")
const CategoryRouter = require("./routers/Category.router")
const SubCategoryRouter = require("./routers/SubCategory.router")
const ProductRouter = require("./routers/Product.router")
const OrderRouter = require("./routers/Order.router")
const GalleryRouter = require("./routers/Gallery.router")
const check_auth =  require("./middlewares/check_auth")
app.use(express.json());
app.use(express.urlencoded());
//public 
app.use("/users", UserRouter)
app.use("/clients", ClientRouter)
app.use("/Providers", ProviderRouter)

//private with token
app.use("/Deliveries", DeliveryRouter)
app.use("/Categories", CategoryRouter)
app.use("/SubCategories", SubCategoryRouter)
app.use("/Products", ProductRouter)
app.use("/Orders", OrderRouter)
app.use("/Galleries", GalleryRouter)
//get Image
app.get("/getImage/:img", function(req,res){
res.sendFile(__dirname+"/storage/"+req.params.img)
})

// setup swagger with node js
app.use("/api-docs", basicAuth({
    users: { "jobgate": "jobgate" },
    challenge: true,
}), swaggerUI.serve, swaggerUI.setup(docs))

// express doesn't consider not found 404 as an error so we need to handle 404 explicitly
// handle 404 error
app.use(function(req, res, next) {
    let err = new Error('Not Found');
       err.status = 404;
       next(err);
   });
   // handle errors
   app.use(function(err, req, res, next) {
    console.log(err);
    
     if(err.status === 404)
      res.status(404).json({message: "Not found", data:null, status:404});
     else 
       res.status(500).json({message: "Something looks wrong :( !!!", data:null, status:500});
   });

app.listen(port, () => {

    console.log(`Example app listening at http://localhost:${port}`)
})

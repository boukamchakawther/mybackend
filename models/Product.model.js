const mongoose = require("mongoose");
// Define a schema
const Schema = mongoose.Schema;
const GallerySchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true,
        required: true
    }
})
const ProductSchema = new Schema({
    refProduct: {
        type: String,
        trim: true,
        required: true
    },
    price: {
        type: String,
        trim: true,
        required: true
    },
    qte: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true,
        required: true
    },
    provider: {
        type: Schema.Types.ObjectID,
        ref: 'Providers'
    },
    subCabtegory: {
        type: Schema.Types.ObjectID,
        ref: 'SubCategories'
    },
    galleries: [GallerySchema],
    orders: [{
        type: Schema.Types.ObjectID,
        ref: 'Orders'
    }]
}, { timestamps: true })
module.exports = mongoose.model("Products", ProductSchema);
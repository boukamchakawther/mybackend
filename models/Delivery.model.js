const mongoose = require("mongoose");
const user = require("./user.model");
const Schema = mongoose.Schema;
const deliverySchema = new Schema({
  num: String,
})
user.discriminator("delivery", deliverySchema);
module.exports = mongoose.model("delivery");
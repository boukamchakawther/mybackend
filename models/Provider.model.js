const mongoose = require("mongoose")
const user = require("./user.model");
const Schema = mongoose.Schema;
const ProviderSchema = new Schema({
   id:String,
   campany: String
})
user.discriminator("Provider", ProviderSchema);
module.exports = mongoose.model("Provider")
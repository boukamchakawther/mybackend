const mongoose = require("mongoose")
const Schema = mongoose.Schema;
const CategorySchema = new Schema({
    name: String,
    description: String,
    image: {
        type:String,
        trim:true,
        required:true
    },
    subcategory: [{ type: Schema.Types.ObjectId, ref: 'SubCategory' }],
})

module.exports = mongoose.model("Category", CategorySchema)


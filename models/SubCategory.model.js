const mongoose = require("mongoose")
const Schema = mongoose.Schema;
const SubCategorySchema = new Schema({
    name: String,
    description: String,
    category: { type: Schema.Types.ObjectId, ref: 'Category' },
    product: [{ type: Schema.Types.ObjectId, ref: 'Product' }],
})

module.exports = mongoose.model("SubCategory", SubCategorySchema)
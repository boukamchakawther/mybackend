const mongoose = require("mongoose");
// Define a schema
const Schema = mongoose.Schema;
const ItemOrderProductSchema = new Schema({
    product: {
        type: Schema.Types.ObjectID,
        ref: 'Products'
    },
    dateL: {
        type: String,
        trim: true,
        required: true
    },
    qte: {
        type: String,
        trim: true,
        required: true
    },
    color: {
        type: String,
        trim: true,
        required: true
    }
})
const OrderSchema = new Schema({
    ref: {
        type: String,
        trim: true,
        required: true
    },
    priceTotal: {
        type: String,
        trim: true,
        required: true
    },
    qteTotal: {
        type: String,
        trim: true,
        required: true
    },
    date: {
        type: String,
        trim: true,
        required: true
    },
    description: {
        type: String,
        trim: true,
        required: true
    },
    products: [ItemOrderProductSchema],
    client:{type:Schema.Types.ObjectId,ref:"Clients"}
}, { timestamps: true })
module.exports = mongoose.model("Orders", OrderSchema);
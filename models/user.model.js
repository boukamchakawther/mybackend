const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Schema = mongoose.Schema;
const baseOption = {
  discriminatorKey: "itemtype", //our disciminator key, could be anything
  collection: "items", // the name of our collection
};
const UserSchema = Schema(
  {
    firstName: { type: String, required: true, unique: true, trim: true },
    lastName: { type: String, required: true },
    phone: { type: Number, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    image: {
      type: String,
      trim: true,
    },
  },
  baseOption,
  { timestamps: true }
);

UserSchema.pre("save", function (next) {
  this.password = bcrypt.hashSync(this.password, 10);
  next();
});
// our base schema: these properties will be shared with our "real" schemas
module.exports = mongoose.model("Users", UserSchema);

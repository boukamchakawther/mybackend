const mongoose = require("mongoose");
const user = require("./user.model");
const Schema = mongoose.Schema;
const ClientSchema = new Schema({
  adressel: {
    type: String,
    trim: true,
    required: true,
  },
  orders: [{ type: Schema.Types.ObjectId, ref: "Orders" }],
});
user.discriminator("Clients", ClientSchema);
module.exports = mongoose.model("Clients");

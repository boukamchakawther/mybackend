const mongoose = require("mongoose")
const Schema = mongoose.Schema;
const GallerySchema = new Schema({
    
    image: String,
    product :{type:Schema.Types.ObjectId,ref:'Product'},
})

module.exports = mongoose.model("Gallery", GallerySchema)
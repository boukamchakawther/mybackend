const SubCategoryController = require("../controllers/SubCategory.controller")
const express = require("express");
const router = express.Router()
router.get("/", SubCategoryController.find)
router.post("/", SubCategoryController.creat)
router.delete("/:id", SubCategoryController.delete)
router.put("/:id", SubCategoryController.update)
router.get("/:id", SubCategoryController.getOne)
router.post("/:findByEmail", SubCategoryController.findByEmail)
module.exports = router
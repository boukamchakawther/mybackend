const OrderController = require("../controllers/Order.controller")
const express = require("express");
const router = express.Router()
router.get("/", OrderController.find)
router.post("/", OrderController.create)
router.delete("/:id", OrderController.delete)
router.put("/:id", OrderController.update)
router.get("/:id", OrderController.getOne)
router.post("/:findByEmail", OrderController.findByEmail)
module.exports = router
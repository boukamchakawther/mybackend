const ClientController = require("../controllers/Client.controller")
const upload= require("../middlewares/uploadFile")
const express = require("express");
const router = express.Router()
router.get("/", ClientController.find)
router.post("/",upload.single("photo"), ClientController.creat)
router.delete("/:id", ClientController.delete)
router.put("/:id", ClientController.update)
router.get("/:id", ClientController.getOne)
router.post("/:findByEmail", ClientController.findByEmail)
module.exports = router
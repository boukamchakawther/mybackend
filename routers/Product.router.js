const ProductController = require("../controllers/Product.controller")
const upload= require("../middlewares/uploadFile")
const express = require("express");
const router = express.Router()
router.get("/", ProductController.find)
router.post("/", upload.single("photo"),ProductController.creat)
router.delete("/:id", ProductController.delete)
router.put("/:id", ProductController.update)
router.get("/:id", ProductController.getOne)
router.post("/:findByEmail", ProductController.findByEmail)
module.exports = router
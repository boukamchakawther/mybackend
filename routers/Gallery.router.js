const GalleryController = require("../controllers/SubCategory.controller")
const upload= require("../middlewares/uploadFile")
const express = require("express");
const router = express.Router()
router.get("/", GalleryController.find)
router.post("/",upload.single("photo"), GalleryController.creat)
router.delete("/:id", GalleryController.delete)
router.put("/:id", GalleryController.update)
router.get("/:id", GalleryController.getOne)
router.post("/:findByEmail", GalleryController.findByEmail)
module.exports = router
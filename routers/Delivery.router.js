const Deliverycontroller = require("../controllers/Delivery.controller")
const express = require("express");
const router = express.Router()
router.get("/", Deliverycontroller.find)
router.post("/", Deliverycontroller.creat)
router.delete("/:id", Deliverycontroller.delete)
router.put("/:id", Deliverycontroller.update)
router.get("/:id", Deliverycontroller.getOne)
router.post("/:findByEmail", Deliverycontroller.findByEmail)
module.exports = router
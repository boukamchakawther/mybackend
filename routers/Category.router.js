const CategoryController = require("../controllers/Category.controller")
const upload= require("../middlewares/uploadFile")
const express = require("express");
const router = express.Router()
router.get("/", CategoryController.find)
router.post("/",upload.single("photo"), CategoryController.create)
router.delete("/:id", CategoryController.delete)
router.put("/",upload.single("photo"), CategoryController.update)
router.get("/:id", CategoryController.getOne)
router.post("/:findByEmail", CategoryController.findByEmail)
module.exports = router
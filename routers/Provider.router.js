const ProviderController = require("../controllers/Provider.controller")
const upload= require("../middlewares/uploadFile")
const express = require("express");
const router = express.Router()
router.get("/", ProviderController.find)
router.post("/",upload.single("photo"), ProviderController.creat)
router.delete("/:id", ProviderController.delete)
router.put("/:id", ProviderController.update)
router.get("/:id", ProviderController.getOne)
router.post("/:findByEmail", ProviderController.findByEmail)
module.exports = router
const usercontroller = require("../controllers/User.controller")
const check_auth= require("../middlewares/check_auth")
const express = require("express");
const router = express.Router()

router.post("/login", usercontroller.login)
router.post("/refreshtoken",check_auth, usercontroller.refreshtoken)
router.post("/logout",usercontroller.logout)


module.exports = router
